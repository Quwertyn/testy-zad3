#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "err.h"
#include <assert.h>

#define NR_PROC 5

int main ()
{
    pid_t pid;
    int i;

    /* tworzenie procesów potomnych */
    for (i = 1; i <= NR_PROC; i++) {
        pid_t father = getpid();
        switch (pid = fork()) {
            case -1:
                printf("Error in fork\n");
            case 0: /* proces potomny */
                assert(getlcapid(getpid(), getpid()) == father);
                break;
            default: /* proces macierzysty */
                if (wait(0) == -1)
                    printf("Error in wait\n");
                return 0;
        }
    }
    return 0;

}